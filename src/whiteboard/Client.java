package whiteboard;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.JFrame;
public class Client extends UnicastRemoteObject implements RemoteObserver{
	private static final long serialVersionUID = 8159122044550890194L;
	protected Client() throws RemoteException {
		super();
	}
	protected static Login programma;
	static Interface inter;
	public static void main(String[] args) {
		try {  
			Registry registry = LocateRegistry.getRegistry(10008);  
			inter = (Interface) registry.lookup("Interface");  
			Client client = new Client();
			inter.addObserverRMI(client);
		} catch (Exception e) {  
			System.err.println("Client exception: " + e.toString());  
			e.printStackTrace();  
		} 
		programma = new Login();
		programma.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}
	@Override
    public void update(Object observable, Object updateMsg)
            throws RemoteException {
        //System.out.println("got message: " + updateMsg);
        BoardFrame.update(null, updateMsg);
    }
}
