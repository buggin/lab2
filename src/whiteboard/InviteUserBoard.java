package whiteboard;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
public class InviteUserBoard extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3464360718488704112L;
	private Container container;
    private GridBagLayout tabella;
    private GridBagConstraints constraints;
    private JLabel codice, proprietario;
    private LinkedList<JButton> buttons;
    static BoardFrame frame;
    public InviteUserBoard () throws RemoteException {
        super ("Lista mie Lavagne");
        container = getContentPane();
        tabella = new GridBagLayout ();
        container.setLayout(tabella);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1000;
        constraints.weighty = 1000;
        codice = new JLabel("   Codice");              
        codice.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
        codice.setForeground(Color.RED);
        proprietario = new JLabel("   Proprietario");              
        proprietario.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
        proprietario.setForeground(Color.RED);
        // ADDCOMPONENT
        addComponent (codice,       0, 0, 1, 1);
        addComponent (proprietario, 0, 1, 1, 1);
        LinkedList<Board> boards = Client.inter.getBoardsList();
        LinkedList<Board> myBoards = new LinkedList<Board>();
        for (Board b : boards) {
        	if(b.getOwner().getName().equals(BoardFrame.currentUser.getName()))
        			myBoards.add(b);
        }
        
        //riempie lista lavagne
        if (!myBoards.isEmpty()) {
        listBoards(myBoards);         
        // EVENTI DEI JBUTTON
        int j = 0;
        for (Board b : myBoards) {
        
            final int cod = (b.getCode());
            buttons.get(j).addActionListener(
                    new ActionListener() {
                        public void actionPerformed( ActionEvent event )  {
                            try {
								new InviteUserList(cod);
								dispose();
							} catch (RemoteException e) {}
                        }
                    });
            j++;
        }
        setResizable( true );
        setSize( 700, 350 );
        setLocationRelativeTo(null);
        setVisible( true ); }
        else
        	JOptionPane.showMessageDialog(this,"Non sei proprietario di nessuna Lavagna!","",JOptionPane.ERROR_MESSAGE);
    }   // END CONSTRUCTOR
    public void listBoards(LinkedList<Board> boards) {
        buttons = new LinkedList<JButton>();
        int i = 0;
        for (Board b: boards) {
            JLabel codice = new JLabel(String.valueOf("        " + b.getCode()));
            JLabel proprietario = new JLabel(b.getOwner().getName());
            JButton entra = new JButton("Entra");
            buttons.add(entra);
            entra.setFont(new Font( "Serif", Font.PLAIN, 20 ));
            addComponent (codice,         i+1, 0, 1, 1);
            addComponent (proprietario,   i+1, 1, 1, 1);
            addComponent (entra,           i+1, 2, 1, 1);
            i++;
        }
    }
    //METODO ADDCOMPONENT
    private void addComponent( Component component, int row, int column, int width, int height ) {
        constraints.gridx = column;
        constraints.gridy = row;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        tabella.setConstraints( component, constraints );
        container.add( component ); 
    }
} // END CLASS