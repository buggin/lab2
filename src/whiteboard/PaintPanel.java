package whiteboard;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.LinkedList;

import javax.swing.JPanel;
class PaintPanel extends JPanel implements MouseMotionListener, MouseListener {

	private static final long serialVersionUID = -1376357310974692038L;
	protected Board board;
	private Color COLOR_FOREGROUND; 
	private Color COLOR_BACKGROUND = Color.WHITE; 
	private int xInit = 10, yInit = 10;
	private int xFinal = 10, yFinal = 10;
	protected transient BufferedImage _bufImage = null;


	public PaintPanel(Board board, Color color, boolean timeline) {
		setPreferredSize(new Dimension(1300, 900)); 
		setBackground(Color.WHITE);
		grabFocus();
		if(!timeline) {
			addMouseMotionListener(this);
			this.board = board;
			try {
				COLOR_FOREGROUND =  Client.inter.colorPermission(BoardFrame.currentUser.getName(), board.getCode());
				setForeground(COLOR_FOREGROUND);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
		Graphics2D g2=(Graphics2D) g;
		if (_bufImage == null) {
			int w = this.getWidth();
			int h = this.getHeight();
			_bufImage = (BufferedImage) this.createImage(w, h); 
			Graphics2D gc = _bufImage.createGraphics(); 
			gc.setColor(COLOR_BACKGROUND);
			gc.fillRect(0, 0, w, h); 
		}
		g2.drawImage(_bufImage, null, 0, 0);
		grabFocus();
	}


	private void drawCurrentShape(Graphics2D g2,boolean firstRun, Color color, boolean timeline) { 
		g2.setColor(color); 
		g2.drawLine(xInit, yInit, xFinal, yFinal);
		if(!timeline){
			if(firstRun)
				sendSegmentServer(true, true);
		}

	}
	private void sendSegmentServer(boolean update, boolean db) {
		Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp nowTime = new java.sql.Timestamp(calendar.getTime().getTime());
		Segment segment;
		try {
			segment = new Segment(xInit, yInit, xFinal, yFinal, nowTime, COLOR_FOREGROUND);
			board.addSegment(segment); 
			if (db == false) 
				segment.setColore(Color.WHITE);
			else 
				BoardFrame.mySegments.get(board).add(segment);
			Client.inter.addSegment(segment, board, BoardFrame.currentUser, db);
			board.setCanged();



		} catch (RemoteException e1) {
			e1.printStackTrace();
		}
	}
	
	
	public void initializeBoard(LinkedList<Segment> lista) {
		_bufImage = null;
		paintComponent(getGraphics());
		for (Segment s : lista){
			xInit = s.getxIn();
			xFinal = s.getxFin();
			yInit = s.getyIn();
			yFinal = s.getyFin();
			drawCurrentShape(_bufImage.createGraphics(),false, s.getColore(),false);
		}
	}
	public void addNewSegment(Segment segment){
		xInit = segment.getxIn();
		xFinal = segment.getxFin();
		yInit = segment.getyIn();
		yFinal = segment.getyFin();
		drawCurrentShape(_bufImage.createGraphics(),false, segment.getColore(), false);
		repaint();

	}
	public void TimelineSingleSegment(Segment segment, boolean first) {
		if (first) {
			_bufImage = null;
			paintComponent(getGraphics());
		}
		xInit = segment.getxIn();
		xFinal = segment.getxFin();
		yInit = segment.getyIn();
		yFinal = segment.getyFin();
		drawCurrentShape(_bufImage.createGraphics(),false, COLOR_FOREGROUND, true);
		repaint();
	}
	public void removeLast(Segment segment) {
		xInit = segment.getxIn();
		xFinal = segment.getxFin();
		yInit = segment.getyIn();
		yFinal = segment.getyFin();
		segment.setColore(Color.WHITE);
		drawCurrentShape(_bufImage.createGraphics(),false, Color.WHITE, false);
		board.addSegment(segment);
		sendSegmentServer(false, false);
		board.setCanged();
		board.removeSegment(segment);
		repaint();

	}

	public void addLast(Segment segment) {
		xInit = segment.getxIn();
		xFinal = segment.getxFin();
		yInit = segment.getyIn();
		yFinal = segment.getyFin();
		drawCurrentShape(_bufImage.createGraphics(),false, COLOR_FOREGROUND, false);
		sendSegmentServer(false, true);
		board.addSegment(segment);
		repaint();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		Point p = e.getPoint();
		System.out.println("Point: "+p);
		finishLine(p);
		startLine(p);
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		startLine(e.getPoint());
		}
	public void startLine(Point e) {
		xInit = (int) e.getX();
		yInit = (int) e.getY();
	}
	public void finishLine(Point e) {
		xFinal = (int) e.getX();
		yFinal = (int) e.getY();
		drawCurrentShape(_bufImage.createGraphics(),true, COLOR_FOREGROUND, false);
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
		startLine(e.getPoint());
	}
	@Override
	public void mouseReleased(MouseEvent e) {	
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
