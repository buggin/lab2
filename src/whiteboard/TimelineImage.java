package whiteboard;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class TimelineImage extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7886773738000045236L;
	private static Container container;
    private static GridBagLayout inter;
    private static GridBagConstraints constraints;
    private static JPanel panel;
    private static JMenuItem avanti;
    private static int counterSegment = 0;
    private static LinkedList<Segment> segments = new LinkedList<Segment>();
    private static PaintPanel paintPanBoard;

    public TimelineImage(int cod, int gg1, int mm1, int aa1, int hh1, int mi1, int gg2, int mm2, int aa2, int hh2, int mi2, boolean visualizzazioneContinua) {
        super ("TimeLine della Lavagna " + cod);
        // CREAZIONE TIMESTAMP
        container = getContentPane();
        inter = new GridBagLayout ();
        container.setLayout(inter);
        constraints = new GridBagConstraints();
        GregorianCalendar cal1 = new GregorianCalendar(aa1, mm1, gg1, hh1, mi1);
        long millis1 = cal1.getTimeInMillis();
        Timestamp momIniziale = new Timestamp(millis1);
        GregorianCalendar cal2 = new GregorianCalendar(aa2, mm2, gg2, hh2, mi2);
        long millis2 = cal2.getTimeInMillis();
        Timestamp momFinale = new Timestamp(millis2);
        try {
            segments = Client.inter.timelineSegments(cod, momIniziale, momFinale);
        } catch (RemoteException e) {}
        if (segments.size() == 0) {
            JOptionPane.showMessageDialog(TimelineImage.this,"Nessun segmento disegnato in questo intervallo di tempo!","",JOptionPane.ERROR_MESSAGE);
            dispose();
        }
        else {
            paintPanBoard = new PaintPanel(null, null, true);
            paintPanBoard.setSize(1300, 900);
            panel = new JPanel();
            panel.add(paintPanBoard);
            panel.setSize(1300, 900);
            constraints.fill = GridBagConstraints.BOTH;
            constraints.weightx = 1000;
            constraints.weighty = 1000;
            addComponent( panel,    0, 0,  1, 1 );
            setVisible(true);
            setSize(1300, 900);
            setLocationRelativeTo(null);

            if (visualizzazioneContinua) {
                boolean first = true;
                while (counterSegment < segments.size()) {
                    Segment s = segments.get(counterSegment);
                    paintPanBoard.TimelineSingleSegment(s, first);
                    counterSegment++;
                    first = false;
                }
                counterSegment = 0;
                segments.clear();
            }
            else {
                avanti = new JMenuItem("Avanti");
                JMenuBar bar = new JMenuBar();
                setJMenuBar(bar);
                bar.add(avanti);
                avanti.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        if (counterSegment < segments.size()) {
                            Segment s = segments.get(counterSegment);
                            boolean first;
                            if(counterSegment == 0)
                                first = true;
                            else
                                first = false; 
                            paintPanBoard.TimelineSingleSegment(s, first);
                            counterSegment++;
                        }
                        else {
                            JOptionPane.showMessageDialog(TimelineImage.this,"Segmenti terminati!","",JOptionPane.ERROR_MESSAGE);
                            dispose();
                            counterSegment = 0;
                            segments.clear();
                        }
                    }});
            }
        }
    }

    //METODO ADDCOMPONENT
    private void addComponent( Component component, int row, int column, int width, int height ) {

        constraints.gridx = column;
        constraints.gridy = row;

        constraints.gridwidth = width;
        constraints.gridheight = height;

        inter.setConstraints( component, constraints );
        container.add( component ); }

}
