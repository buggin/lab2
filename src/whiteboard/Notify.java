package whiteboard;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
public class Notify extends JFrame {
	private static final long serialVersionUID = -7373134568264662289L;
	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;
	private JLabel codice, richiedente;
	private JButton[] buttonsSI;
	private JButton[] buttonsNO;
	public Notify () throws RemoteException {
		super ("Notifiche");
		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1000;
		constraints.weighty = 1000;
		codice = new JLabel("   Codice");              
		codice.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
		codice.setForeground(Color.RED);
		richiedente = new JLabel("Richiedente");              
		richiedente.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
		richiedente.setForeground(Color.RED);
		// ADDCOMPONENT
		addComponent (codice,       0, 0, 1, 1);
		addComponent (richiedente,  0, 1, 1, 1);
		LinkedList<String> emails = Client.inter.usersRequests(BoardFrame.currentUser.getName());         
		LinkedList<Integer> codes = Client.inter.boardsRequests(BoardFrame.currentUser.getName());         
		//riempie lista richieste
		if(!codes.isEmpty()){
			mostraRichieste(codes, emails);         
			// EVENTI DEI JBUTTON
			// BUTTON 'ACCETTA'
			for (int i = 0; i<codes.size(); i++) {
				final int cod = codes.get(i);
				final String email = emails.get(i);
				buttonsSI[i].addActionListener(
						new ActionListener() {
							public void actionPerformed( ActionEvent event ) {
								try {
									Client.inter.acceptRequest(cod, email);
									dispose();
								} catch (RemoteException e) {}
							}});
			}
			// BUTTON 'RIFIUTA'
			for (int i = 0; i<codes.size(); i++) {
				final int cod = codes.get(i);
				final String email = emails.get(i);
				buttonsNO[i].addActionListener(
						new ActionListener() {
							public void actionPerformed( ActionEvent event ) {
								try {
									Client.inter.refuseRequest(BoardFrame.currentUser.getName(), cod, email);
									dispose();
								} catch (RemoteException e) {}
							}});
			}
			setResizable( true );
			setSize( 700, 350 );
			setLocationRelativeTo(null);
			setVisible( true );}
		else {
		JOptionPane.showMessageDialog(this,"Nessuna richiesta!","",JOptionPane.ERROR_MESSAGE);}
	}   // END CONSTRUCTOR
	public void mostraRichieste(LinkedList<Integer> cods, LinkedList<String> emails) {
		buttonsSI = new JButton[cods.size()];
		buttonsNO = new JButton[cods.size()];
		for (int i=0; i<cods.size();i++) {
			JLabel codice = new JLabel(String.valueOf("        " + cods.get(i)));
			JLabel proprietario = new JLabel(emails.get(i));
			JButton si = new JButton("Accetta");
			JButton no = new JButton("Rifiuta");
			buttonsSI[i] = si;
			buttonsNO[i] = no;
			si.setFont(new Font( "Serif", Font.PLAIN, 20 ));
			no.setFont(new Font( "Serif", Font.PLAIN, 20 ));
			addComponent (codice,         i+1, 0, 1, 1);
			addComponent (proprietario,   i+1, 1, 1, 1);
			addComponent (si,             i+1, 2, 1, 1);
			addComponent (no,             i+1, 3, 1, 1);
		}
	}
	//METODO ADDCOMPONENT
	private void addComponent( Component component, int row, int column, int width, int height ) {
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}
} // END CLASS