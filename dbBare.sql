CREATE DATABASE  IF NOT EXISTS `databaselaboratorioii` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `databaselaboratorioii`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: databaselaboratorioii
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesso`
--

DROP TABLE IF EXISTS `accesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesso` (
  `mail` varchar(50) NOT NULL,
  `codice` int(11) NOT NULL,
  `colore` varchar(45) NOT NULL,
  PRIMARY KEY (`mail`,`codice`),
  KEY `Codice_idx` (`codice`),
  CONSTRAINT `Codice` FOREIGN KEY (`codice`) REFERENCES `lavagna` (`codice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Mail` FOREIGN KEY (`mail`) REFERENCES `registro` (`Mail`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `lavagna`
--

DROP TABLE IF EXISTS `lavagna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavagna` (
  `codice` int(11) NOT NULL AUTO_INCREMENT,
  `proprietario` varchar(45) NOT NULL,
  PRIMARY KEY (`codice`),
  KEY `Proprietario_idx` (`proprietario`),
  CONSTRAINT `Proprietario` FOREIGN KEY (`proprietario`) REFERENCES `registro` (`Mail`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifiche`
--

DROP TABLE IF EXISTS `notifiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche` (
  `richiedente` varchar(45) NOT NULL,
  `proprietario` varchar(45) NOT NULL,
  `lavagna` int(11) NOT NULL,
  PRIMARY KEY (`richiedente`,`lavagna`),
  KEY `dasda_idx` (`proprietario`),
  KEY `asdaf_idx` (`lavagna`),
  CONSTRAINT `asdaf` FOREIGN KEY (`lavagna`) REFERENCES `lavagna` (`codice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dasda` FOREIGN KEY (`proprietario`) REFERENCES `registro` (`Mail`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `dsa` FOREIGN KEY (`richiedente`) REFERENCES `registro` (`Mail`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `mail` varchar(50) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `tratto`
--

DROP TABLE IF EXISTS `tratto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tratto` (
  `mail` varchar(50) NOT NULL,
  `momento` datetime(6) NOT NULL,
  `coorInizialeX` int(11) NOT NULL,
  `coorFinaleX` int(11) NOT NULL,
  `coorInizialeY` int(11) NOT NULL,
  `coorFinaleY` int(11) NOT NULL,
  `codice` int(11) NOT NULL,
  `colore` varchar(45) NOT NULL,
  PRIMARY KEY (`mail`,`momento`),
  KEY `Codice_idx` (`codice`),
  CONSTRAINT `Codice878` FOREIGN KEY (`codice`) REFERENCES `lavagna` (`codice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Mail88` FOREIGN KEY (`mail`) REFERENCES `registro` (`Mail`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28 17:40:42
