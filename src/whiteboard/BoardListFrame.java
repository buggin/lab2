package whiteboard;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
public class BoardListFrame extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3464360718488704112L;
	private Container container;
    private GridBagLayout tabella;
    private GridBagConstraints constraints;
    private JLabel codice, proprietario;
    private LinkedList<JButton> buttons;
    static BoardFrame frame;
    Desktop desktop;

    public BoardListFrame () throws RemoteException {
        super ("Lista Lavagne");
        container = getContentPane();
        tabella = new GridBagLayout ();
        container.setLayout(tabella);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1000;
        constraints.weighty = 1000;
        codice = new JLabel("   Codice");              
        codice.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
        codice.setForeground(Color.RED);
        proprietario = new JLabel("   Proprietario");              
        proprietario.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
        proprietario.setForeground(Color.RED);
        // ADDCOMPONENT
        addComponent (codice,       0, 0, 1, 1);
        addComponent (proprietario, 0, 1, 1, 1);
        LinkedList<Board> boards = Client.inter.getBoardsList();
        //riempie lista lavagne
        if (!boards.isEmpty()) {
        listBoards(boards);         
        // EVENTI DEI JBUTTON
        int j = 0;
        for (Board b : boards) {
        
            final int cod = (b.getCode());
            final String email = b.getOwner().getName();
            final Color color = Client.inter.colorPermission(BoardFrame.currentUser.getName(), b.getCode());
            buttons.get(j).addActionListener(
                    new ActionListener() {
                        public void actionPerformed( ActionEvent event )  {
                            if (color != null) {
                                try {
                                    Board board = Client.inter.requestBoard(cod,BoardFrame.currentUser);
                                    // ORA DEVO METTERE LA LAVAGNA NEL TAB E POSSO DISEGNARCI
                                    boolean esit = frame.addTabOldBoard(board, color);
                                    if (esit){
                                    	dispose();
                                    }
                                    else 
                                        JOptionPane.showMessageDialog( BoardListFrame.this, "Lavagna gia' aperta!", "", JOptionPane.PLAIN_MESSAGE );
                                } catch (RemoteException e) {}
                            }
                            else {    // NON POSSO DISEGNARCI ANCORA, DEVO RICHEIDERE L'ISCRIZIONE
                                try {
                                    if(!Client.inter.notify(BoardFrame.currentUser.getName(), email, cod))
                                        JOptionPane.showMessageDialog( BoardListFrame.this, "Richiesta gia' inviata!", "", JOptionPane.ERROR_MESSAGE );
                                    else{
                                    	if (Desktop.isDesktopSupported() 
        									    && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
										URI mailt = new URI("mailto:"+email +"?subject=Richiesta%20accesso%20lavagna%20condivisa%20"+cod+"&body=Ciao%2C%20vorrei%20accedere%20alla%20tua%20lavagna%20condivisa%20"+cod+"!%0D%0AAccedi%20al%20client%20per%20rispondere%20alla%20richiesta%20%3A)%0D%0AGrazie!%0D%0A"+BoardFrame.currentUser.getName());
										desktop.mail(mailt);
                                    	JOptionPane.showMessageDialog( BoardListFrame.this, "Richiesta inviata!", "", JOptionPane.PLAIN_MESSAGE );
                                    	}
                                    	else{
    										JOptionPane.showMessageDialog(BoardListFrame.this,"Non sono riuscito ad aprire il client di posta. Controlla sia configurato correttamente","OK",JOptionPane.ERROR_MESSAGE);
                                    	}
                                    }
                                    dispose();
                                } catch (RemoteException e) {} catch (URISyntaxException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}  
                            }
                        }
                    });
            j++;
        }
        setResizable( true );
        setSize( 700, 350 );
        setLocationRelativeTo(null);
        setVisible( true ); }
        else 
        	JOptionPane.showMessageDialog(this,"Nessuna lavagna presente!","",JOptionPane.ERROR_MESSAGE);
    }   // END CONSTRUCTOR
    public void listBoards(LinkedList<Board> boards) {
        buttons = new LinkedList<JButton>();
        int i = 0;
        for (Board b: boards) {
            JLabel codice = new JLabel(String.valueOf("        " + b.getCode()));
            JLabel proprietario = new JLabel(b.getOwner().getName());
            JButton join = new JButton("join");
            buttons.add(join);
            join.setFont(new Font( "Serif", Font.PLAIN, 20 ));
            addComponent (codice,         i+1, 0, 1, 1);
            addComponent (proprietario,   i+1, 1, 1, 1);
            addComponent (join,           i+1, 2, 1, 1);
            i++;
        }
    }
    //METODO ADDCOMPONENT
    private void addComponent( Component component, int row, int column, int width, int height ) {
        constraints.gridx = column;
        constraints.gridy = row;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        tabella.setConstraints( component, constraints );
        container.add( component ); 
    }
} // END CLASS