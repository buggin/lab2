package whiteboard;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class InfoBoard extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4628615678021420066L;
	private static Container container;
    private static GridBagLayout inter;
    private static GridBagConstraints constraints;
    private static JLabel codice, moment1, moment2;
    private static JLabel sep1, sep2, sep3, sep4; 
    private static JLabel trat1, trat2;
    private static JLabel punt1, punt2;
    private static JTextField codiceField;
    private static JTextField gg1, mm1, aa1, hh1, mi1;
    private static JTextField gg2, mm2, aa2, hh2, mi2;
    private static JTextField utente;
    private static JButton visualizza;
    private static String[] names = {"Determinato Utente", "Tutti gli Utenti"};
    private static JRadioButton scelta[];
    private static ButtonGroup gruppo;
    private static boolean determinatoUtente;
    public InfoBoard() {

        super("Statistiche di una Lavagna - Inserire dati");

        container = getContentPane();
        inter = new GridBagLayout ();
        container.setLayout(inter);
        constraints = new GridBagConstraints();

        codice = new JLabel("  Inserire il codice della Lavagna");
        moment1 = new JLabel("  Inserire il momento iniziale, nel formato GG/MM/AAAA-HH:MM");
        moment2 = new JLabel("  Inserire il momento finale, nello stesso formato");

        sep1 = new JLabel("  /");
        sep2 = new JLabel("  /");
        sep3 = new JLabel("  /");
        sep4 = new JLabel("  /");

        trat1 = new JLabel("  -");
        trat2 = new JLabel("  -");

        punt1 = new JLabel("  :");
        punt2 = new JLabel("  :");

        codiceField = new JTextField("", 10);    
        codiceField.setEditable( true );
        codiceField.setFont( new Font( "Serif", Font.PLAIN, 15) );

        ItemHandler handler = new ItemHandler();
        scelta = new JRadioButton[names.length];
        for (int count = 0; count < names.length; count++) {
            scelta[count] = new JRadioButton(names[count]);
            scelta[count].addActionListener(handler);
        }
        gruppo = new ButtonGroup();
        for(int count = 0; count < names.length; count++) {
            gruppo.add(scelta[count]);
        }

        scelta[0].setSelected(true);
        determinatoUtente = true;

        gg1 = new JTextField("", 10);    
        gg1.setEditable( true );
        gg1.setFont( new Font( "Serif", Font.PLAIN, 15) );

        mm1 = new JTextField("", 10);    
        mm1.setEditable( true );
        mm1.setFont( new Font( "Serif", Font.PLAIN, 15) );

        aa1 = new JTextField("", 10);    
        aa1.setEditable( true );
        aa1.setFont( new Font( "Serif", Font.PLAIN, 15) );

        hh1 = new JTextField("", 10);    
        hh1.setEditable( true );
        hh1.setFont( new Font( "Serif", Font.PLAIN, 15) );

        mi1 = new JTextField("", 10);    
        mi1.setEditable( true );
        mi1.setFont( new Font( "Serif", Font.PLAIN, 15) );

        gg2 = new JTextField("", 10);    
        gg2.setEditable( true );
        gg2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        mm2 = new JTextField("", 10);    
        mm2.setEditable( true );
        mm2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        aa2 = new JTextField("", 10);    
        aa2.setEditable( true );
        aa2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        hh2 = new JTextField("", 10);    
        hh2.setEditable( true );
        hh2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        mi2 = new JTextField("", 10);    
        mi2.setEditable( true );
        mi2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        utente = new JTextField("", 10);    
        mi2.setEditable( true );
        mi2.setFont( new Font( "Serif", Font.PLAIN, 15) );

        visualizza = new JButton("VISUALIZZA");

        visualizza.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (codiceField.getText().equals("") | gg1.getText().equals("") | mm1.getText().equals("") | aa1.getText().equals("") | hh1.getText().equals("") | mi1.getText().equals("") | gg2.getText().equals("") | mm2.getText().equals("") | aa2.getText().equals("") | hh2.getText().equals("") | mi2.getText().equals("")){
                    JOptionPane.showMessageDialog(InfoBoard.this,"Compilare tutti i campi richiesti!","Errore",JOptionPane.ERROR_MESSAGE);
                }
                else {
                    boolean ok = true;
                    int cod = 0;
                    int gg1n = 0;
                    int mm1n = 0;
                    int aa1n = 0;
                    int hh1n = 0;
                    int mi1n = 0;
                    int gg2n = 0;
                    int mm2n = 0;
                    int aa2n = 0;
                    int hh2n = 0;
                    int mi2n = 0;
                    try {
                        cod = Integer.parseInt(codiceField.getText()); 
                        gg1n = Integer.parseInt(gg1.getText());
                        mm1n = Integer.parseInt(mm1.getText());
                        aa1n = Integer.parseInt(aa1.getText());
                        hh1n = Integer.parseInt(hh1.getText());
                        mi1n = Integer.parseInt(mi1.getText());
                        gg2n = Integer.parseInt(gg2.getText());
                        mm2n = Integer.parseInt(mm2.getText());
                        aa2n = Integer.parseInt(aa2.getText());
                        hh2n = Integer.parseInt(hh2.getText());
                        mi2n = Integer.parseInt(mi2.getText());
                    }
                    catch (java.lang.NumberFormatException e) {
                        JOptionPane.showMessageDialog(InfoBoard.this,"Inserire numeri!","Errore",JOptionPane.ERROR_MESSAGE);
                        ok = false;
                    }
                    if (gg1n>31 | mm1n>12 | hh1n>24 | mi1n>60 |gg2n>31 | mm2n>12 | hh2n>24 | mi2n>60) {
                        JOptionPane.showMessageDialog(InfoBoard.this,"Date o orari non corretti!","Errore",JOptionPane.ERROR_MESSAGE);
                        ok = false;
                    }
                    if (ok) {
                        LinkedList<Board> boards = new LinkedList<Board>();
                        try {
                            boards = Client.inter.getBoardsList();
                        } catch (RemoteException e1) {}
                        LinkedList<Integer> myBoards = new LinkedList<Integer>();
                        try {                       
                            for (Board b : boards) {
                                Color color = Client.inter.colorPermission(BoardFrame.currentUser.getName(), b.getCode());
                                if (color != null)
                                    myBoards.add(b.getCode());
                            }
                            if (myBoards.contains(cod)){
                                mm1n--;
                                mm2n--;
                                GregorianCalendar cal1 = new GregorianCalendar(aa1n, mm1n, gg1n, hh1n, mi1n);
                                long millis1 = cal1.getTimeInMillis();
                                Timestamp momIniziale = new Timestamp(millis1);

                                GregorianCalendar cal2 = new GregorianCalendar(aa2n, mm2n, gg2n, hh2n, mi2n);
                                long millis2 = cal2.getTimeInMillis();
                                Timestamp momFinale = new Timestamp(millis2);

                                if (determinatoUtente == true) {
                                    if(utente.getText().length()!=0) {
                                        try {
                                            LinkedList<String> info = Client.inter.infoBoard(cod, momIniziale, momFinale, utente.getText());
                                            new ShowInfoBoard(info);
                                        } catch (RemoteException e) {}
                                    }
                                    else
                                        JOptionPane.showMessageDialog(InfoBoard.this,"Inserire un Utente!","Errore",JOptionPane.ERROR_MESSAGE);
                                }
                                else {
                                    LinkedList<String> info = Client.inter.infoBoard(cod, momIniziale, momFinale, null);
                                    new ShowInfoBoard(info);
                                }
                                dispose();
                            }
                            else {
                                JOptionPane.showMessageDialog(InfoBoard.this,"Questo codice non corrisponde a una lavagna a cui hai acccesso!","Errore",JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (RemoteException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }});

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1000;
        constraints.weighty = 1000;

        addComponent( codice,         0, 0, 1 ,1 );
        addComponent( codiceField,    0, 1, 1, 1 );
        addComponent( moment1,        1, 0, 1, 1 );
        addComponent( gg1,            1, 1, 1, 1 ); 
        addComponent( sep1,           1, 2, 1, 1 );
        addComponent( mm1,            1, 3, 1, 1 );
        addComponent( sep2,           1, 4, 1, 1 );
        addComponent( aa1,            1, 5, 1, 1 );
        addComponent( trat1,          1, 6, 1, 1 );
        addComponent( hh1,            1, 7, 1, 1 );
        addComponent( punt1,          1, 8, 1, 1 );
        addComponent( mi1,            1, 9, 1, 1 );
        addComponent( moment2,        2, 0, 1, 1 );
        addComponent( gg2,            2, 1, 1, 1 ); 
        addComponent( sep3,           2, 2, 1, 1 );
        addComponent( mm2,            2, 3, 1, 1 );
        addComponent( sep4,           2, 4, 1, 1 );
        addComponent( aa2,            2, 5, 1, 1 );
        addComponent( trat2,          2, 6, 1, 1 );
        addComponent( hh2,            2, 7, 1, 1 );
        addComponent( punt2,          2, 8, 1, 1 );
        addComponent( mi2,            2, 9, 1, 1 );
        addComponent(scelta[0],       4, 0, 1, 1 );
        addComponent(scelta[1],       3, 0, 1, 1 );
        addComponent(utente,          4, 1, 1, 1 );
        addComponent( visualizza,     4, 11, 1, 1);




        setVisible(true);
        setSize(1000, 250);
        setLocationRelativeTo(null);
    }

    private class ItemHandler implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(event.getSource() == scelta[0]) {
                determinatoUtente = true;
                utente.setVisible(true);
            }
            else {
                determinatoUtente = false;
                utente.setVisible(false);
            }
        }
    }

    //METODO ADDCOMPONENT
    private void addComponent( Component component, int row, int column, int width, int height ) {

        constraints.gridx = column;
        constraints.gridy = row;

        constraints.gridwidth = width;
        constraints.gridheight = height;

        inter.setConstraints( component, constraints );
        container.add( component ); }


}
