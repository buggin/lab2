package whiteboard;
import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.LinkedList;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;


public class DeleteBoard extends JFrame {
	private static final long serialVersionUID = 3879597622178357629L;
	private JTextField textField;
	public DeleteBoard() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{201, 253, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);

		JLabel lblCancellaUnaLavagna = new JLabel("Cancella una Lavagna (Causa il logout dell'utente)");
		lblCancellaUnaLavagna.setForeground(Color.RED);
		GridBagConstraints gbc_lblCancellaUnaLavagna = new GridBagConstraints();
		gbc_lblCancellaUnaLavagna.gridwidth = 2;
		gbc_lblCancellaUnaLavagna.insets = new Insets(0, 0, 5, 0);
		gbc_lblCancellaUnaLavagna.gridx = 0;
		gbc_lblCancellaUnaLavagna.gridy = 0;
		getContentPane().add(lblCancellaUnaLavagna, gbc_lblCancellaUnaLavagna);

		JLabel lblInserireIlCodice = new JLabel("Inserisci il Codice della ");
		GridBagConstraints gbc_lblInserireIlCodice = new GridBagConstraints();
		gbc_lblInserireIlCodice.insets = new Insets(0, 0, 5, 5);
		gbc_lblInserireIlCodice.gridx = 0;
		gbc_lblInserireIlCodice.gridy = 2;
		getContentPane().add(lblInserireIlCodice, gbc_lblInserireIlCodice);

		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.gridheight = 2;
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);

		JLabel lblLavagnaCheSi = new JLabel(" Lavagna che vuoi cancellare:");
		GridBagConstraints gbc_lblLavagnaCheSi = new GridBagConstraints();
		gbc_lblLavagnaCheSi.anchor = GridBagConstraints.EAST;
		gbc_lblLavagnaCheSi.insets = new Insets(0, 0, 5, 5);
		gbc_lblLavagnaCheSi.gridx = 0;
		gbc_lblLavagnaCheSi.gridy = 3;
		getContentPane().add(lblLavagnaCheSi, gbc_lblLavagnaCheSi);

		JButton btnCancella = new JButton("CANCELLA");
		GridBagConstraints gbc_btnCancella = new GridBagConstraints();
		gbc_btnCancella.gridx = 1;
		gbc_btnCancella.gridy = 4;
		getContentPane().add(btnCancella, gbc_btnCancella);


		btnCancella.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				boolean ok = true;
				int cod = 0;
				try {
					cod = Integer.parseInt(textField.getText()); }
				catch (java.lang.NumberFormatException e) {
					JOptionPane.showMessageDialog(DeleteBoard.this,"Inserire un codice!","Errore",JOptionPane.ERROR_MESSAGE);
					ok = false;
				}
				if (ok) {
					LinkedList<Board> boards = new LinkedList<Board>();
					try {
						boards = Client.inter.getBoardsList();
					} catch (RemoteException e1) {}
					LinkedList<Integer> myBoards = new LinkedList<Integer>();
					try {						
						for (Board b : boards) {
							Color color = Client.inter.colorPermission(BoardFrame.currentUser.getName(), b.getCode());
							if (color != null)
								myBoards.add(b.getCode());
						}
						if (myBoards.contains(cod)){
							Client.inter.dropTable(cod, BoardFrame.currentUser);
							JOptionPane.showMessageDialog(DeleteBoard.this,"Lavagna rimossa!","OK",JOptionPane.PLAIN_MESSAGE);
							Login.frame.logOut();
							if(BoardFrame.currentBoards.contains(cod))
								BoardFrame.currentBoards.remove(new Integer(cod));
							dispose();
						}
						else {
							JOptionPane.showMessageDialog(DeleteBoard.this,"Questo codice non corrisponde a una lavagna a cui hai acccesso!","Errore",JOptionPane.ERROR_MESSAGE);
						}
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}});

		setVisible(true);
		setSize(350, 135);
		setLocationRelativeTo(null);

	}



}
