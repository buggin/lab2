package whiteboard;
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;
public class RegisterFrame extends JFrame {
	private static final long serialVersionUID = 427857649481553170L;
	private static JTextField mail;
	private static JPasswordField password;
	public RegisterFrame() {
		// INTERFACCIA GRAFICA
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{180, 224, 0};
		gridBagLayout.rowHeights = new int[]{68, 48, 45, 45, 42, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		JLabel lblRegistrazione = new JLabel("Registrazione");
		lblRegistrazione.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblRegistrazione.setForeground(Color.RED);
		GridBagConstraints gbc_lblRegistrazione = new GridBagConstraints();
		gbc_lblRegistrazione.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblRegistrazione.gridwidth = 2;
		gbc_lblRegistrazione.insets = new Insets(0, 0, 5, 0);
		gbc_lblRegistrazione.gridx = 0;
		gbc_lblRegistrazione.gridy = 0;
		getContentPane().add(lblRegistrazione, gbc_lblRegistrazione);
		JLabel lblInserireEmail = new JLabel("Inserire e-Mail");
		lblInserireEmail.setFont(new Font("Lucida Grande", Font.PLAIN, 19));
		GridBagConstraints gbc_lblInserireEmail = new GridBagConstraints();
		gbc_lblInserireEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblInserireEmail.gridx = 0;
		gbc_lblInserireEmail.gridy = 2;
		getContentPane().add(lblInserireEmail, gbc_lblInserireEmail);
		mail = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		getContentPane().add(mail, gbc_textField);
		mail.setColumns(10);
		JLabel lblInserirePassword = new JLabel("Inserire Password");
		lblInserirePassword.setFont(new Font("Lucida Grande", Font.PLAIN, 19));
		GridBagConstraints gbc_lblInserirePassword = new GridBagConstraints();
		gbc_lblInserirePassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblInserirePassword.gridx = 0;
		gbc_lblInserirePassword.gridy = 3;
		getContentPane().add(lblInserirePassword, gbc_lblInserirePassword);
		password = new JPasswordField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.fill = GridBagConstraints.BOTH;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 3;
		getContentPane().add(password, gbc_textField_1);
		password.setColumns(10);
		JButton btnConferma = new JButton("Conferma");
		btnConferma.setFont(new Font("Lucida Grande", Font.PLAIN, 17));
		GridBagConstraints gbc_btnConferma = new GridBagConstraints();
		gbc_btnConferma.fill = GridBagConstraints.VERTICAL;
		gbc_btnConferma.anchor = GridBagConstraints.EAST;
		gbc_btnConferma.gridx = 1;
		gbc_btnConferma.gridy = 5;
		getContentPane().add(btnConferma, gbc_btnConferma);
		// FINE INTERFACCIA GRAFICA
		// EVENTI
		btnConferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						char[] pas = password.getPassword();
						String passwordDecodificata = decodificaPassword(pas);
						if (mail.getText().equals("") || passwordDecodificata.equals(""))
							JOptionPane.showMessageDialog( RegisterFrame.this, "Compilare tutti i campi richiesti!", "Errore di registrazione", JOptionPane.PLAIN_MESSAGE );
						else
							try {
								aggiungiCliente(mail.getText(), passwordDecodificata);
							} catch (RemoteException e) {}
					}
				}
				);
		setSize( 600, 350 );
		setVisible( true );
		setLocationRelativeTo(null);
	}
	public static String decodificaPassword (char[] passwordToDec) {
		String pass = new String();
		for (int i = 0; i < passwordToDec.length; i++) {
			pass += passwordToDec[i];
		}
		return pass;
	}
	public void aggiungiCliente (String email, String pass) throws RemoteException {
		boolean registrOk = Client.inter.addUser(email, pass);
		if(registrOk){
			JOptionPane.showMessageDialog( RegisterFrame.this, "Registrazione effettuata!", "OK", JOptionPane.PLAIN_MESSAGE );
			char[] pas = password.getPassword();
			String passwordDecodificata = decodificaPassword(pas);
			BoardFrame.currentUser = new User(mail.getText(), passwordDecodificata);
			dispose();}
		else{
			JOptionPane.showMessageDialog( RegisterFrame.this, "Email esistente!", "Errore", JOptionPane.PLAIN_MESSAGE );
		}
	}
}
