package whiteboard;
import java.awt.Color;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Properties;
public class ServerDB{
	static Statement stm;

	public ServerDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver" );
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}  
		stm = createStm();
	}
	private Statement createStm(){
		String url = "jdbc:mysql://localhost:3306/databaselaboratorioii";
		Statement stm1 = null;
		try {
			Connection con= null;
			Properties connectionProps = new Properties();
			connectionProps.put("user", "root");
			connectionProps.put("password", "12345");
			con = DriverManager.getConnection(url,connectionProps);

			stm1 = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stm1;
	}
	public void addSegment(Segment segment, Board board, User user){
		try {
			int xInit, yInit, xFinal, yFinal;
			xInit = segment.getxIn();
			yInit = segment.getyIn();
			xFinal = segment.getxFin();
			yFinal = segment.getyFin();
			stm.executeUpdate("INSERT INTO tratto(mail,momento,coorInizialeX,coorFinaleX,"
					+ "coorInizialeY,coorFinaleY,codice,colore) VALUES('"+
					user.getName()+"','"+ segment.getMomento()+"','"+
					xInit+"','"+xFinal+"','"+yInit+"','"+yFinal+"','"
					+ ""+board.getCode()+"','"+segment.getColore().getRGB()+"')");
		} catch (SQLException e) { 
			e.printStackTrace();
		}
	}
	public LinkedList<Board> getBoardsList(){
		LinkedList<Board> bl = new LinkedList<>();
		Statement stm1 = createStm();
		try {
			ResultSet rs = stm1.executeQuery("SELECT * FROM lavagna;"); 
			while(rs.next()){
				int cod = rs.getInt("codice");
				String propr = rs.getString("proprietario");
				bl.add(new Board(new User(propr,getPswd(propr)), cod ,requestBoard(cod).getSegments()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return bl;
	}
	public String getPswd(String mail) throws SQLException{
		String propr = "";
		ResultSet rs = stm.executeQuery("SELECT * FROM registro WHERE mail = '"+mail+"';"); 
		while(rs.next()){
			propr = rs.getString("password");
		}
		return propr;
	}
	public Board requestBoard(int codB) throws RemoteException {
		Board board;
		LinkedList<Segment> linkedList = new LinkedList<Segment>();
		String owner="";
		String pswd="";
		try {
			ResultSet rs = stm.executeQuery("SELECT * FROM tratto WHERE codice = "+codB);
			while(rs.next()) {
				int ix=rs.getInt("coorInizialeX");
				int iy=rs.getInt("coorInizialeY");
				int fx=rs.getInt("coorFinaleX");
				int fy=rs.getInt("coorFinaleY");
				java.sql.Timestamp tm = rs.getTimestamp("momento");
				int col = rs.getInt("colore");
				Color colore = new Color(col);
				linkedList.add(new Segment(ix,iy,fx,fy,tm,colore));
				owner = rs.getString(1);
			}
			rs = stm.executeQuery("SELECT * FROM registro WHERE mail = '"+owner+"';");		
			if(rs.next()){
				pswd= rs.getString(2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		board = new Board(new User(owner,pswd), codB, linkedList);
		return board;
	}

	public boolean addUser(String email, String password) throws RemoteException {
		int numberOfStatements=0;
		try {
			numberOfStatements = stm.executeUpdate("INSERT INTO registro(mail,password) VALUES ('"+email+"','"+password+"')");
		}catch (SQLException e ){
			e.printStackTrace();}
		if(numberOfStatements==1)
			return true;
		else
			return false;
	}
	public LinkedList<User> loadUsers(){
		LinkedList<User> users = new LinkedList<User>();
		ResultSet rs;
		try {
			rs = stm.executeQuery("SELECT * FROM registro;");
			while(rs.next()){
				String mail = rs.getString("mail");
				String psw = rs.getString("password");
				users.add(new User(mail,psw));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	public Color checkPermission(String mail, int codice)
			throws RemoteException {
		ResultSet rs;
		Color colore = null;
		try {
			rs = stm.executeQuery("SELECT * FROM accesso WHERE mail = '"+mail+"' and codice = '"+codice+"';");
			while(rs.next()){
				int col = rs.getInt("colore");
				colore = new Color(col);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return colore;
	}
	public boolean notifyAccess(String emailRichiedente, String emailProprietario, int codice)
			throws RemoteException {
		try {
			stm.executeUpdate("INSERT INTO notifiche VALUES ('"+emailRichiedente+"','"+emailProprietario+"','"+codice+"')");
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	public LinkedList<String> usersRequests(String email)
			throws RemoteException {
		ResultSet rs;
		LinkedList<String> richiedenti = new LinkedList<String>();
		try {
			rs = stm.executeQuery("SELECT richiedente FROM notifiche WHERE proprietario = '"+email+"';");
			while(rs.next()){
				richiedenti.add(rs.getString("richiedente"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return richiedenti;
	}
	public LinkedList<Integer> boardsRequests(String email)
			throws RemoteException {
		ResultSet rs;
		LinkedList<Integer> richiesti= new LinkedList<Integer>();
		try {
			rs = stm.executeQuery("SELECT lavagna FROM notifiche WHERE proprietario = '"+email+"';");
			while(rs.next()){
				richiesti.add(rs.getInt("lavagna"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return richiesti;
	}
	public void acceptRequest(Color colore, int codice, String mail)
			throws RemoteException {
		try {
			stm.executeUpdate("INSERT INTO accesso(colore,mail,codice) VALUES ('"
					+ colore.getRGB() +"','" + mail + "','" +codice+"')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		deleteRequest(codice, mail);
	}
	public void deleteRequest(int codice, String mail)
			throws RemoteException {
		try {
			stm.executeUpdate("DELETE FROM notifiche WHERE lavagna = '"+codice+"' and richiedente = '"+mail+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public int getCodeBoard(String proprietario) {
		int codeLastBoard = 0;
		try {
			stm.executeUpdate("INSERT INTO lavagna(proprietario) VALUES ('"+proprietario+"')");			
			ResultSet rs = stm.executeQuery("SELECT LAST_INSERT_ID()"); 
			if(rs.next())
				codeLastBoard = rs.getInt(1);
			stm.executeUpdate("INSERT INTO accesso(colore,mail,codice) VALUES ('"
					+ Color.RED.getRGB()+"','" + proprietario + "','" +codeLastBoard+"')");
		}catch (SQLException e ){
			e.printStackTrace();}
		return codeLastBoard;
	}
	int discoverNumber(int codice) {
		ResultSet rs;
		int i = 0;
		try {
			rs = stm.executeQuery("SELECT COUNT(*) as num FROM accesso WHERE codice = '"+codice+"';");
			while(rs.next()){
				i = rs.getInt("num")+1;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	boolean isOwner(int cod, User currUser){
		ResultSet rs;
		try {
			rs = stm.executeQuery("SELECT proprietario FROM lavagna WHERE codice = '"+cod+"';");
			while(rs.next()){
				if(rs.getString("proprietario").equals(currUser.getName()))
					return true;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public void dropTable(int cod) {
		try {
			stm.executeUpdate("DELETE FROM lavagna WHERE codice = '"+cod+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void dropAccess(int cod, User currentUser) {
		try {
			stm.executeUpdate("DELETE FROM accesso WHERE codice = '"+cod+"' and mail = '"+currentUser.getName()+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static LinkedList<Segment> timelineSegments(int cod, Timestamp momIniziale, Timestamp momFinale) {

		ResultSet rs;
		LinkedList<Segment> lista= new LinkedList<Segment>();
		try {
			rs = stm.executeQuery("SELECT coorInizialeX, coorFinaleX, coorInizialeY, coorFinaleY FROM tratto WHERE Codice = '"+cod+"' AND Momento BETWEEN '"+momIniziale+"' AND '"+momFinale+"';");
			while(rs.next()){
				Segment nuovo = new Segment(rs.getInt("coorInizialeX"), rs.getInt("coorInizialeY"), rs.getInt("coorFinaleX"), rs.getInt("coorFinaleY" ),null, null);
				lista.add(nuovo);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public static int infoBoardNum(int cod, Timestamp momIniziale, Timestamp momFinale, String text) {

		ResultSet rs;
		int info = 0;
		if (text == null) {
			try {
				rs = stm.executeQuery("SELECT count(*) as num FROM tratto WHERE Codice = '"+cod+"' AND Momento BETWEEN '"+momIniziale+"' AND '"+momFinale+"';");
				while(rs.next()){
					info = rs.getInt("num");
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return info;
		}
		else {
			try {
				rs = stm.executeQuery("SELECT count(*) as num FROM tratto WHERE Codice = '"+cod+"' AND Mail = '"+text+"' AND Momento BETWEEN '"+momIniziale+"' AND '"+momFinale+"';");
				while(rs.next()){
					info = rs.getInt("num");
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return info;
		}
	}



	public static String infoBoardMostActive(int cod, String text) {

		ResultSet rs;
		String info= new String();

		try {
			rs = stm.executeQuery("SELECT Mail FROM tratto X GROUP BY Mail, Codice HAVING Codice = '"+cod+"' and count(*) > all(SELECT count(*) FROM tratto GROUP BY Mail, Codice HAVING Codice = '"+cod+"' and Mail <> X.Mail)");
			while(rs.next()){
				info = rs.getString("Mail");
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return info;

	}
	public LinkedList<User> getUserList(String owner, int lavagna) {

		LinkedList<User> users = new LinkedList<User>();
		Statement stm1 = createStm();
		try {
			ResultSet rs = stm1.executeQuery("SELECT Mail, Password FROM registro WHERE Mail <> '"+owner+"' and Mail NOT IN (SELECT Mail FROM accesso  WHERE Codice = '"+lavagna+"') "); 
			while(rs.next()){
				String mail = rs.getString("mail");
				String password = rs.getString("password");
				users.add(new User(mail,password));
			}
		} catch (SQLException e) {
			e.printStackTrace();}

			return users;
		}
	public void removeSegment(Segment segment) {
		try {
			stm.executeUpdate("DELETE FROM tratto WHERE Momento = '"+segment.getMomento()+"' and coorInizialeX = '"+segment.getxIn()+"' and coorInizialeY = '"+segment.getyIn()+"' and coorFinaleX = '"+segment.getxFin()+"' and coorFinaleY = '"+segment.getyFin()+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	}
