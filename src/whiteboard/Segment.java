package whiteboard;
import java.awt.Color;
import java.io.Serializable;
import java.sql.Timestamp;
public class Segment implements Serializable {
	private static final long serialVersionUID = 8965541008444751230L;
	private int xIn, xFin, yIn, yFin;
	private java.sql.Timestamp momento;
	private Color colore;
	public Color getColore() {
		return colore;
	}
	public void setColore(Color colore) {
		this.colore = colore;
	}
	public java.sql.Timestamp getMomento() {
		return momento;
	}
	public void setMomento(java.sql.Timestamp momento) {
		this.momento = momento;
	}
	public void setxIn(int xIn) {
		this.xIn = xIn;
	}
	public void setxFin(int xFin) {
		this.xFin = xFin;
	}
	public void setyIn(int yIn) {
		this.yIn = yIn;
	}
	public void setyFin(int yFin) {
		this.yFin = yFin;
	}
	public Segment(int xInit, int yInit, int xFinal, int yFinal, Timestamp nowTime, Color colore) {
		this.xIn = xInit;
		this.xFin = xFinal;
		this.yIn = yInit;
		this.yFin = yFinal;
		setMomento(nowTime);
		setColore(colore);		
	}
	public int getxIn() {
		return xIn;
	}
	public int getxFin() {
		return xFin;
	}
	public int getyIn() {
		return yIn;
	}
	public int getyFin() {
		return yFin;
	}
	
}
