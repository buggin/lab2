package whiteboard;
import java.io.Serializable;
public class User implements Serializable {
	private String name,psw;
	User(String name,String psw){
		setName(name);
		setPsw(psw);
	}
	private static final long serialVersionUID = -7020619477594468968L;
	public String getName() {
		return name;
	}
	private void setName(String name) {
		this.name = name;
	}
	public String getPsw() {
		return psw;
	}
	private void setPsw(String psw) {
		this.psw = psw;
	}
	public String toString(){
		return new String("User "+getName()+" Pswd:"+this.getPsw());
	}
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof User))
			return false;
		else{
			User u = (User) obj;
			if(this.getName().equals(u.getName()))
					if(this.getPsw().equals(u.getPsw()))
						return true;
					else
						return false;
			else
				return false;
		}
	}
	@Override
	public int hashCode(){
		String code = getName()+getPsw();
		int cod = code.hashCode();
		return cod;
	}
}
