package whiteboard;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
public class InviteUserList extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3464360718488704112L;
	private Container container;
    private GridBagLayout tabella;
    private GridBagConstraints constraints;
    private JLabel mail;
    private LinkedList<JButton> buttons;
    static BoardFrame frame;
    Desktop desktop;
    public InviteUserList (int cod) throws RemoteException {
        super ("Lista Utenti");
        container = getContentPane();
        tabella = new GridBagLayout ();
        container.setLayout(tabella);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1000;
        constraints.weighty = 1000;
        mail = new JLabel("   Mail");              
        mail.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
        mail.setForeground(Color.RED);
        // ADDCOMPONENT
        addComponent (mail,       0, 0, 1, 1);
        LinkedList<User> users = Client.inter.getUserList(BoardFrame.currentUser.getName(), cod);
        //riempie lista clienti
        listBoards(users);         
        // EVENTI DEI JBUTTON
        int j = 0;
        for (User u : users) {
        
            final int codice = cod;
            final String email = u.getName();
            buttons.get(j).addActionListener(
                    new ActionListener() {
                        public void actionPerformed( ActionEvent event )  {
                            try {
								Client.inter.acceptRequest(codice, email);
								JOptionPane.showMessageDialog(InviteUserList.this,"Invito effettuato!","OK",JOptionPane.PLAIN_MESSAGE);
								if (Desktop.isDesktopSupported() 
									    && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
									String body = "Ciao,%20sei%20stato%20invitato%20da%20"+BoardFrame.currentUser.getName()+"%20ad%20accedere%20alla%20sua%20lavagna%20condivisa!%0D%0AEnjoy!";
									  URI mailto = new URI("mailto:"+email+"?subject=Invito%20lavagna%20condivisa%20"+codice+"&body="+body);
									  desktop.mail(mailto);
									} else {
										JOptionPane.showMessageDialog(InviteUserList.this,"Non sono riuscito ad aprire il client di posta. Controlla sia configurato correttamente","OK",JOptionPane.ERROR_MESSAGE);
									}
								dispose();
							} catch (RemoteException e) {} catch (URISyntaxException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
                        }
                    });
            j++;
        }
        setResizable( true );
        setSize( 700, 350 );
        setLocationRelativeTo(null);
        setVisible( true );
    }   // END CONSTRUCTOR
    
    public void listBoards(LinkedList<User> users) {
        buttons = new LinkedList<JButton>();
        int i = 0;
        for (User u: users) {
            JLabel codice = new JLabel(String.valueOf("        " + u.getName()));
            JButton invita = new JButton("Invita");
            buttons.add(invita);
            invita.setFont(new Font( "Serif", Font.PLAIN, 20 ));
            addComponent (codice,         i+1, 0, 1, 1);
            addComponent (invita,           i+1, 2, 1, 1);
            i++;
        }
    }
    //METODO ADDCOMPONENT
    private void addComponent( Component component, int row, int column, int width, int height ) {
        constraints.gridx = column;
        constraints.gridy = row;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        tabella.setConstraints( component, constraints );
        container.add( component ); 
    }
} // END CLASS