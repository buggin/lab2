package whiteboard;
import java.awt.Color;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.LinkedList;

public interface Interface extends Remote {
	/**
	 * Ritorna la lista di tutte le lavagne presenti nel Database
	 * @return LinkedList<Board>
	 * @throws RemoteException
	 */
	public LinkedList<Board> getBoardsList() throws RemoteException;

	/**
	 * Crea una nuova Lavagna
	 * @param currentUser 
	 * @return Board
	 * @throws RemoteException
	 */
	public Board addBoard(User currentUser) throws RemoteException;   
    
	/**
	 * Aggiunge un segmento alla lavagna
	 * @param segment
	 * @param board
	 * @param currentUser 
	 * @throws RemoteException
	 */
	public void addSegment(Segment segment, Board board, User currentUser, boolean db) throws RemoteException;  

	/**
	 * Controlla se l'utente e' registrato ed e' attivo
	 * @param user
	 * @return
	 * @throws RemoteException
	 */
	public boolean checkUser(User user) throws RemoteException;

	/**
	 * Controlla se la mail esiste nel DB
	 * @param email
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean checkEmail(String email) throws RemoteException;  
               
	/**
	 * Per quella mail, controlla che la password sia corretta
	 * @param psw
	 * @return booelan
	 * @throws RemoteException
	 */
	public boolean checkPwd(String psw, String email) throws RemoteException;  
                   

	/**
	 * Richiede una specifica lavagna al server
	 * @param codB
	 * @param currentUser 
	 * @return Board
	 * @throws RemoteException
	 */
	public Board requestBoard(int codB, User currentUser) throws RemoteException;  
                   
	/**
	 * Aggiunge un utente nel db
	 * @param email
	 * @param password
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean addUser(String email, String password) throws RemoteException;  
    
	/**
	 * Controlla se quella mail ha il diritto di disegnare su quella lavagna
	 * @param mail
	 * @param codice
	 * @return Colore associato alla mail per quella lavagna
	 * @throws RemoteException
	 */
	public Color colorPermission(String mail, int codice) throws RemoteException;   
 
	/**
	 * email richiede l'iscrizione alla lavagna codice al proprietario emailProprietario (con una notifica in 'notifiche' e una mail)
	 * @param email indica l'utente
	 * @param emailProprietario indica il proprietario della lavagna
	 * @param codice lavagna a cui si vuole iscrivere
	 * @return 
	 * @throws RemoteException
	 */
	public boolean notify(String email, String emailProprietario, int codice) throws RemoteException;  

	/**
	 * Per quella mail, restituisce (dal db) le mail degli utenti che gli richiedono l'accesso a una lavagna
	 * @param email
	 * @return LinkedList<String>
	 * @throws RemoteException
	 */
	public LinkedList<String> usersRequests(String email) throws RemoteException;  

	/**
	 * Per quella mail, restituisce (dal db) le lavagne per cui ha delle richieste (in ordine con le mail date dal metodo precedente)
	 * @param email
	 * @return LinkedList<Integer>
	 * @throws RemoteException
	 */
	public LinkedList<Integer> boardsRequests(String email) throws RemoteException; 
 
	/**
	 * La currentMail(proprietario) accetta la richiesta della mail di iscriversi alla lavagna codice (va comunicato all'utente mail e al db)
	 * @param codice
	 * @param mail
	 * @throws RemoteException
	 */
	public void acceptRequest(int codice, String mail) throws RemoteException; 
  
	/**
	 * La currentMail (proprietario) rifiuta la richiesta della mail di iscriversi alla lavagna codice (va comunicato all'utente mail e al db)
	 * @param currentMail
	 * @param codice
	 * @param mail
	 * @throws RemoteException
	 */
	public void refuseRequest(String currentMail, int codice, String mail) throws RemoteException; 
 
	/**
	 * 
	 * @param lavagna
	 * @return
	 * @throws RemoteException
	 */
	public Color discoverColor(int lavagna)  throws RemoteException;
	
	/**
	 * 
	 * @param cod
	 * @param currentUser
	 * @throws RemoteException
	 */
	public void dropTable(int cod, User currentUser) throws RemoteException;
	
	/**
	 * 
	 * @param o
	 * @throws RemoteException
	 */
	public void addObserverRMI(RemoteObserver o) throws RemoteException;
	
	/**
	 * 
	 * @param cod
	 * @param momIniziale
	 * @param momFinale
	 * @return
	 * @throws RemoteException
	 */
	public LinkedList<Segment> timelineSegments(int cod, Timestamp momIniziale, Timestamp momFinale) throws RemoteException;
	
	/**
	 * 
	 * @param cod
	 * @param momIniziale
	 * @param momFinale
	 * @param text
	 * @return
	 * @throws RemoteException
	 */
	public LinkedList<String> infoBoard(int cod, Timestamp momIniziale, Timestamp momFinale, String text) throws RemoteException;
	
	/**
	 * 
	 * @param owner
	 * @param lavagna
	 * @return
	 * @throws RemoteException
	 */
	public LinkedList<User> getUserList(String owner, int lavagna) throws RemoteException;
	
	/**
	 * 
	 * @param segment
	 * @throws RemoteException
	 */
	public void removeSegment(Segment segment) throws RemoteException;

}
