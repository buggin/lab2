package whiteboard;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
public class BoardFrame extends JFrame {

	protected static User currentUser;
	protected static LinkedList<Board> currentBoards = new LinkedList<Board>();  
	private  static final long serialVersionUID = -1702920019804970744L;
	private  static Container container;
	protected  static JTabbedPane tabbedPane;
	private  static JMenuBar bar;
	private JMenu file;
	private  static JMenuItem listaLavagne, addBoard, notifiche, eliminaLavagna, logout, timeline, statistiche, invita;
	private static Hashtable<Board, PaintPanel> panelsActive = new Hashtable<Board, PaintPanel>();
	protected static Hashtable<Board, LinkedList<Segment>> mySegments = new Hashtable<Board, LinkedList<Segment>>();
	protected static Hashtable<Board, LinkedList<Segment>> removedSegments = new Hashtable<Board, LinkedList<Segment>>();

	public BoardFrame() {
		super("Lavagna Collaborativa di "+BoardFrame.currentUser.getName());
		container = getContentPane();
		tabbedPane = new JTabbedPane();
		container.add( tabbedPane );
		// BARRA IN ALTO
		bar = new JMenuBar();
		setJMenuBar(bar);
		file = new JMenu("File");
		bar.add(file);
		listaLavagne = new JMenuItem("Lista Lavagne");  
		addBoard = new JMenuItem("Aggiungi Lavagna");  
		notifiche = new JMenuItem("Notifiche");      
		logout = new JMenuItem("Logout");
		statistiche = new JMenuItem("Statistiche");
		file.add(listaLavagne);
		file.add(addBoard);
		file.add(notifiche);
		eliminaLavagna = new JMenuItem("Elimina Lavagna");
		invita = new JMenuItem("Invita");
		timeline= new JMenuItem("Timeline");
		file.add(timeline);
		file.add(statistiche);
		file.add(eliminaLavagna);
		file.add(invita);
		file.add(logout);
		// EVENTI
		listaLavagne.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						try {
							new BoardListFrame();

						} catch (RemoteException e) {}
					}});
		addBoard.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						try {  
							Board board = Client.inter.addBoard(BoardFrame.currentUser);
							addTabNewBoard(board);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}});
		notifiche.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						try {
							new Notify();
						} catch (RemoteException e) {}
					}});
		eliminaLavagna.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						new DeleteBoard();
					}});
		logout.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						int exit = JOptionPane.showConfirmDialog(null, "Vuoi effettuare il logout? ","Attenzione!",JOptionPane.YES_NO_OPTION);
						if (exit == 0) {
							BoardFrame.currentUser = null;
							currentBoards.clear();
							dispose();
							new Login();
						}
					}});
		timeline.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						new TimelineCod();
					}});
		statistiche.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						new InfoBoard();
					}});
		invita.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						try {
							new InviteUserBoard();
						} catch (RemoteException e) {}
					}});
		setSize( 1300, 900 );
		setVisible( true );
		setLocationRelativeTo(null);
	} 
	// end constructor

	public  void addTabNewBoard(Board board) {
		currentBoards.add(board);
		JPanel panel = new JPanel();
		JButton indietro = new JButton("INDIETRO");
		JButton avanti = new JButton("AVANTI");
		panel.add( indietro, BorderLayout.NORTH );
		panel.add( avanti, BorderLayout.SOUTH );
		PaintPanel paintPanBoard = new PaintPanel(board,new Color(-65536), false);
		paintPanBoard.setSize(1300, 900);
		panel.add(paintPanBoard);
		tabbedPane.addTab("Lavagna #" + board.getCode(), panel);
		tabbedPane.grabFocus();
		tabbedPane.transferFocus();
		panelsActive.put(board, paintPanBoard);
		LinkedList<Segment> lista1 = new LinkedList<Segment>();
		mySegments.put(board, lista1);
		LinkedList<Segment> lista2 = new LinkedList<Segment>();
		removedSegments.put(board, lista2);
		//EVENTI DEI JBUTTON
		final PaintPanel paint = paintPanBoard;
		final Board lavagna = board;
		indietro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (mySegments.get(lavagna).size()>0) {
					Segment segment = mySegments.get(lavagna).getLast();
					paint.removeLast(segment);
					mySegments.get(lavagna).remove(segment);
					removedSegments.get(lavagna).add(segment);
					try {
						Client.inter.removeSegment(segment);
					} catch (RemoteException e) {}
				}
			}});
		avanti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if(removedSegments.get(lavagna).size()>0) {
					Segment segment = removedSegments.get(lavagna).getLast();
					paint.addLast(segment);
					removedSegments.get(lavagna).remove(segment);
				}
			}});
	}
	public boolean addTabOldBoard(Board board, Color color){
		if (!currentBoards.contains(board)) {
			currentBoards.add(board);
			JPanel panel = new JPanel();
			JButton indietro = new JButton("INDIETRO");
			JButton avanti = new JButton("AVANTI");
			panel.add( indietro, BorderLayout.NORTH );
			panel.add( avanti, BorderLayout.SOUTH );
			PaintPanel paintPanBoard = new PaintPanel(board,color,false);
			paintPanBoard.setSize(1300, 900);
			panel.add(paintPanBoard);
			tabbedPane.addTab("Lavagna #" + board.getCode(), panel);
			paintPanBoard.initializeBoard(board.getSegments());
			panelsActive.put(board, paintPanBoard);
			LinkedList<Segment> lista1 = new LinkedList<Segment>();
			mySegments.put(board, lista1);
			LinkedList<Segment> lista2 = new LinkedList<Segment>();
			removedSegments.put(board, lista2);
			//EVENTI DEI JBUTTON
			final PaintPanel paint = paintPanBoard;
			final Board lavagna = board;
			indietro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					if (mySegments.get(lavagna).size()>0) {
						Segment segment = mySegments.get(lavagna).getLast();
						paint.removeLast(segment);
						mySegments.get(lavagna).remove(segment);
						removedSegments.get(lavagna).add(segment);
						try {
							Client.inter.removeSegment(segment);
						} catch (RemoteException e) {}
					}
				}});
			avanti.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					if(removedSegments.get(lavagna).size()>0) {
						Segment segment = removedSegments.get(lavagna).getLast();
						paint.addLast(segment);
						removedSegments.get(lavagna).remove(segment);
					}
				}});
			return true;
		}
		else{
			return false;
		}
	}
	public boolean refreshTab(Board board, Color color){
		if (!currentBoards.contains(board)) {
			currentBoards.add(board);
			PaintPanel paintPanBoard = new PaintPanel(board,color, false);
			paintPanBoard.setSize(1300, 900);
			paintPanBoard.initializeBoard(board.getSegments());
			return true;
		}
		else{
			return false;
		}
	}
	public void logOut() {
		BoardFrame.currentUser = null;
		currentBoards.clear();
		dispose();
		new Login();
	}

	public static void update(Observable o, Object arg) {
		if(arg instanceof Board){
			Board b = (Board)arg;
			PaintPanel p = panelsActive.get(b);
			if(p != null) 
				p.addNewSegment(b.getSegments().getLast()); 
		}
	}

}