package whiteboard;
import java.awt.Color;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

public class Server extends Observable implements Interface {
	private static ServerDB serverDB;

	private static Hashtable<String, User> allUsers = new Hashtable<String, User>();

	private static LinkedList<Board> allBoards = new LinkedList<Board>();

	private static ArrayList<Color> colori = new ArrayList<Color>();

	private class WrappedObserver implements Observer, Serializable {
		private static final long serialVersionUID = 1L;
		private RemoteObserver ro = null;
		public WrappedObserver(RemoteObserver ro) {
			this.ro = ro;
		}
		@Override
		public void update(Observable o, Object arg) {
			try {
				ro.update(o.toString(), arg);
			} catch (RemoteException e) {
				o.deleteObserver(this);
			}
		}
	}

	protected Server()  {
		serverDB = new ServerDB();
		allBoards = loadBoardsList();
	}
	
	public static void main(String args[]) throws RemoteException {  
		try {  
			Server obj = new Server();  
			Interface stub = (Interface) UnicastRemoteObject.exportObject(obj, 0);  
			Registry registry = LocateRegistry.createRegistry(10008); 
			registry.rebind("Interface", stub);
			System.err.println("Server ready");
		} catch (Exception e) {  
			System.err.println("Server exception: " + e.toString());  
			e.printStackTrace();  
		}  
	}
	@Override
	public LinkedList<Board> getBoardsList() throws RemoteException {
		return allBoards;
	}
	private LinkedList<Board> loadBoardsList(){
		return serverDB.getBoardsList();
	}
	@Override
	public void addSegment(Segment segment, Board board, User user, boolean db) throws RemoteException {
		if (db)
			serverDB.addSegment(segment, board, user);
		if(countObservers()>0){
			setChanged();
			notifyObservers(board);
		}
	}

	@Override 
	public boolean checkEmail(String email) throws RemoteException {
		if(allUsers.isEmpty()){
			LinkedList<User> listAllUsers = new LinkedList<User>();
			listAllUsers = serverDB.loadUsers();
			allUsers = loadUsersFromList(listAllUsers);
		}
		return allUsers.containsKey(email);
	}
	private Hashtable<String, User> loadUsersFromList(LinkedList<User> listAllUsers) {
		for(User u: listAllUsers){
			if(!allUsers.containsValue(u)){
				allUsers.put(u.getName(), u);
			}
		}
		return allUsers;
	}
	@Override
	public boolean checkPwd(String psw, String email) throws RemoteException {
		boolean pass = false;
		for(User u: allUsers.values()){
			if(u.getName().equals(email))
				if(u.getPsw().equals(psw)){
					pass=true;
				}
		}
		return pass;
	}

	@Override
	public Board requestBoard(int codB, User user) throws RemoteException {
		Board b = serverDB.requestBoard(codB);
		return b;
	}

	@Override
	public boolean addUser(String email, String password) throws RemoteException {
		if(checkEmail(email))
			return false;
		else{
			if(serverDB.addUser(email, password)){
				allUsers.put(email, new User(email,password));
				return true;}
			else{
				return false;
			}}
	}
	@Override
	public Color colorPermission(String mail, int codice) throws RemoteException{
		return serverDB.checkPermission(mail, codice);
	}
	@Override
	public boolean notify(String email, String emailProprietario, int codice) throws RemoteException{
		return serverDB.notifyAccess(email, emailProprietario, codice);
	}
	@Override
	public LinkedList<String> usersRequests(String email) throws RemoteException {
		return serverDB.usersRequests(email);
	}
	@Override
	public LinkedList<Integer> boardsRequests(String email) throws RemoteException {
		return serverDB.boardsRequests(email);
	}
	@Override
	public void acceptRequest(int codice, String mail)
			throws RemoteException {
		if(colori.isEmpty())
			inizializeColors();
		Color colore = discoverColor(codice);
		serverDB.acceptRequest(colore,codice, mail);
	}
	private void inizializeColors() {
		colori.add(Color.YELLOW);
		colori.add(Color.RED);
		colori.add(Color.ORANGE);
		colori.add(Color.BLUE);
		colori.add(Color.GREEN);
		colori.add(Color.BLACK);
		colori.add(Color.CYAN);
		colori.add(Color.GRAY);
		colori.add(Color.LIGHT_GRAY);
		colori.add(Color.MAGENTA);
		colori.add(Color.PINK);
	}
	public Color discoverColor(int lavagna) {
		if(colori.isEmpty())
			inizializeColors();
		int coloriUsati = serverDB.discoverNumber(lavagna);
		Color col = colori.get(coloriUsati%11);
		return col;
	}
	@Override
	public void refuseRequest(String currentMail, int codice, String mail)
			throws RemoteException {
		serverDB.deleteRequest(codice, mail);
	}  
	@Override
	public Board addBoard(User currentUser) throws RemoteException {
		int code = serverDB.getCodeBoard(currentUser.getName());
		Board board = new Board(currentUser, code,new LinkedList<Segment>());
		allBoards.add(board);
		return board; 

	}
	@Override
	public boolean checkUser(User user) throws RemoteException {
		boolean ok = false;
		ok = checkEmail(user.getName());
		if(ok){
			checkPwd(user.getPsw(), user.getName());
			if(ok){
				if(allUsers.containsValue(user)){
					return true;
				}
				else{
					return false;
				}
			}else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	@Override
	public void dropTable(int cod, User currentUser) throws RemoteException {
		Board questa = null;
		if(serverDB.isOwner(cod, currentUser)){
			serverDB.dropTable(cod);
			for(Board b : allBoards){
				if(b.getCode()==cod)
					questa = b;
			}
			allBoards.remove(questa);
		}else{
			serverDB.dropAccess(cod,currentUser);
			for(Board b : allBoards){
				if(b.getCode()==cod)
					questa = b;
			}
		}
	}
	public void addObserverRMI(RemoteObserver o) throws RemoteException {
		WrappedObserver mo = new WrappedObserver(o);
		addObserver(mo);

	}
	@Override
	public LinkedList<Segment> timelineSegments(int cod, Timestamp momIniziale, Timestamp momFinale) throws RemoteException {
		LinkedList<Segment> lista = ServerDB.timelineSegments(cod, momIniziale, momFinale);
		return lista;
	}
	@Override
	public LinkedList<String> infoBoard(int cod, Timestamp momIniziale, Timestamp momFinale, String text) throws RemoteException {
		int num = ServerDB.infoBoardNum(cod, momIniziale, momFinale, text);
		String utent = ServerDB.infoBoardMostActive(cod, text);
		String numString = String.valueOf(num);
		LinkedList<String> ret = new LinkedList<String>();
		ret.add(numString);
		ret.add(utent);
		return ret;
	}
	@Override
	public LinkedList<User> getUserList(String owner, int lavagna) throws RemoteException {
		return serverDB.getUserList(owner, lavagna);
	}
	@Override
	public void removeSegment(Segment segment) throws RemoteException {
		serverDB.removeSegment(segment);

	}
}
