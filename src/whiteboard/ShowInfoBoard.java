package whiteboard;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ShowInfoBoard extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8456676708616285584L;
	private static Container container;
    private static GridBagLayout inter;
    private static GridBagConstraints constraints;
    private static JLabel numSeg1, numSeg2, utentActive1, utentActive2;

    public ShowInfoBoard(LinkedList<String> info) {

        super("Statistiche di una Lavagna");

        container = getContentPane();
        inter = new GridBagLayout ();
        container.setLayout(inter);
        constraints = new GridBagConstraints();
        
        numSeg1 = new JLabel("  Numero di Segmenti nella lavagna: ");
        numSeg2 = new JLabel(info.get(0));
        utentActive1 = new JLabel(" Utente piu attivo: ");
        utentActive2 = new JLabel(info.get(1));
        
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1000;
        constraints.weighty = 1000;
        
        addComponent( numSeg1,         0, 0, 1 ,1 );
        addComponent( numSeg2,         0, 1, 1, 1 );
        addComponent( utentActive1,    1, 0, 1, 1 );
        addComponent( utentActive2,    1, 1, 1, 1 ); 
        

        setVisible(true);
        setSize(300, 150);
        setLocationRelativeTo(null);
        
        
    }
    
    //METODO ADDCOMPONENT
        private void addComponent( Component component, int row, int column, int width, int height ) {

            constraints.gridx = column;
            constraints.gridy = row;

            constraints.gridwidth = width;
            constraints.gridheight = height;

            inter.setConstraints( component, constraints );
            container.add( component ); }

}