package whiteboard;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Observable;
public class Board extends Observable implements Serializable {
	private static final long serialVersionUID = -5292572196807945074L;
	private final User owner;
	private LinkedList<Segment> segments = new LinkedList<Segment>();
	public Board(User owner, int code, LinkedList<Segment> segment) {
		super();
		this.owner = owner;
		this.code= code;
		this.segments = segment;
	}
	public User getOwner() {
		return owner;
	}
	public void addSegment(Segment segment) {
		this.segments.add(segment);
	}
	
	public void removeSegment(Segment segment) {
		this.segments.remove(segment);
	}
	
	public LinkedList<Segment> getSegments() {
		return segments;
	}
	private int code;

	public int getCode() {
		return code;
	}
	@Override
	public int hashCode(){
		return getCode();
	}
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Board))
			return false;
		else{
			Board b = (Board) obj;
			if(this.getCode()==b.getCode())
				return true;
			else 
				return false;
		}
	}
	public boolean identic(Board b){
		if(this.equals(b))
			if(this.segments.size()==b.segments.size())
				return true;
			else
				return false;
		else
			return false;
	}
	public void setCanged(){
		setChanged();
		notifyObservers(getSegments().getLast());
	}
	
	
	

}
