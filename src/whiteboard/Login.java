package whiteboard;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
public class Login extends JFrame {
	private static final long serialVersionUID = -7481286903766630847L;
	private JTextField mail;
	private JPasswordField password;
	static BoardFrame frame;
	public Login() {
		super("Login");
		// INTERFACCIA GRAFICA
		setResizable(true);
		setSize(1000, 500);
		setLocationRelativeTo(null);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 204, 437, 599, 0 };
		gridBagLayout.rowHeights = new int[] { 103, 53, 52, 51, 35, 43, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);
		JLabel lblNewLabel = new JLabel("New label");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		lblNewLabel.setIcon(new ImageIcon(
				"Logo.png"));
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		JLabel lblNewLabel_1 = new JLabel("   Inserire e-Mail");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		mail = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		getContentPane().add(mail, gbc_textField);
		mail.setColumns(10);
		JLabel lblNewLabel_2 = new JLabel("   Inserire Password");
		lblNewLabel_2.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);
		password = new JPasswordField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.gridwidth = 2;
		gbc_textField_1.fill = GridBagConstraints.BOTH;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		getContentPane().add(password, gbc_textField_1);
		password.setColumns(10);
		JButton accedi = new JButton("Accedi");
		GridBagConstraints gbc_btnAccedi = new GridBagConstraints();
		gbc_btnAccedi.fill = GridBagConstraints.BOTH;
		gbc_btnAccedi.insets = new Insets(0, 0, 5, 0);
		gbc_btnAccedi.gridx = 2;
		gbc_btnAccedi.gridy = 3;
		getContentPane().add(accedi, gbc_btnAccedi);
		JLabel lblNonSeiAncora = new JLabel("Non sei ancora registrato?");
		GridBagConstraints gbc_lblNonSeiAncora = new GridBagConstraints();
		gbc_lblNonSeiAncora.anchor = GridBagConstraints.EAST;
		gbc_lblNonSeiAncora.insets = new Insets(0, 0, 0, 5);
		gbc_lblNonSeiAncora.gridx = 1;
		gbc_lblNonSeiAncora.gridy = 5;
		getContentPane().add(lblNonSeiAncora, gbc_lblNonSeiAncora);
		JButton registra = new JButton("Registrati");
		registra.setForeground(Color.RED);
		GridBagConstraints gbc_btnRegistrati = new GridBagConstraints();
		gbc_btnRegistrati.fill = GridBagConstraints.BOTH;
		gbc_btnRegistrati.gridx = 2;
		gbc_btnRegistrati.gridy = 5;
		getContentPane().add(registra, gbc_btnRegistrati);
		setVisible(true);
		// FINE INTERFACCIA GRAFICA
		
		// EVENTI
		accedi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (mail.getText().equals("") || RegisterFrame.decodificaPassword(password.getPassword()).equals(""))
					JOptionPane.showMessageDialog(Login.this,"Compilare tutti i campi richiesti!","Errore di registrazione",JOptionPane.PLAIN_MESSAGE);
				else {
					boolean exists = false;
					try {
						exists = checkAccount(mail.getText(), RegisterFrame.decodificaPassword(password.getPassword()));
					} catch (RemoteException e1) {
					}
					if (exists) {
						User user = new User(mail.getText(), RegisterFrame.decodificaPassword(password.getPassword()));
						BoardFrame.currentUser = user;
						frame = new BoardFrame();
						BoardListFrame.frame = frame;
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
						dispose();
					} else {
						JOptionPane.showMessageDialog(Login.this,
								"E-mail o password errati!",
								"Errore di Accesso", JOptionPane.PLAIN_MESSAGE);
					}
				}

			}
		});
		registra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RegisterFrame();
			}
		});
	} // END CONSTRUCTOR
	private boolean checkAccount(String email, String password)
			throws RemoteException {
		return Client.inter.checkUser(new User(email,password));
	}

} // END CLASS